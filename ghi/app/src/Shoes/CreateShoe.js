import React, { useState, useEffect } from 'react';


const CreateShoe = () => {
    const [manufacturer, setManufacturer] = useState("")
    const [name, setName] = useState("")
    const [color, setColor] = useState("")
    const [url, setUrl] = useState("")
    const [bin, setBin] = useState("")
    const [bins, setBins] = useState([])

    const handleManufacturerChange = event => {
        setManufacturer(event.target.value)
    }

    const handleNameChange = event => {
        setName(event.target.value)
    }

    const handleColorChange = event => {
        setColor(event.target.value)
    }

    const handleUrlChange = event => {
        setUrl(event.target.value)
    }

    const handleBinChange = event => {
        setBin(event.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.manufacturer = manufacturer
        data.name = name
        data.color = color
        data.url = url
        data.bin = bin

        const url = 'http://localhost:8080/api/shoes/'
        const options = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, options)
        if(response.ok) {
            const newShoe = await response.json()

            setManufacturer("")
            setName("")
            setColor("")
            setUrl("")
            setBin("")
        }
    }

    const fetchData = async () => {
        const wardrobeUrl = 'http://localhost:8100/api/bins/';
        const response = await fetch(wardrobeUrl)

        if(response.ok) {
            const data = await response.json()
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create new shoe</h1>
            <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" className="form-control" value={manufacturer} />
                <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" className="form-control" value={name} />
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="Color" required type="text" className="form-control" value={color} />
                <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleUrlChange} placeholder="url" required type="link"  className="form-control" value={url} />
                <label htmlFor="url">URL</label>
            </div>
            <div className="mb-3">
                <select onChange={handleBinChange} required className="form-select" value={bin}>
                <option>Select a bin</option>
                {bins.map(bin => {
                    return (
                        <option key={bin.id} value={bin.id}>
                            {bin.name}
                        </option>
                    );
                })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
)
}

export default CreateShoe
