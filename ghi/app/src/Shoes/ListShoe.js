import React, { useState, useEffect } from 'react';

const ListShoe = () => {

    const  [shoes, setShoes] = useState([])

    // const getShoes = async () => {
    //         const url = 'http://localhost:8080/api/shoes'
    //         const response = await fetch(url);

    //         if(response.ok) {
    //             const data = await response.json()
    //             const shoes = data.shoes
    //             console.log("shoes shoes shoes", shoes)
    //             setShoes(shoes)
    //         }
    // }

    // useEffect(() => {
    //     getShoes()
    // },[])

    useEffect(() => {
        const intervalId = setInterval(() => {
            async function getShoes() {
                try {
                    const response = await fetch('http://localhost:8080/api/shoes/')
                    console.log(response)
                    const data = await response.json();
                    console.log("this is the data". data)
                    setShoes(data);
                } catch (error) {
                    console.log(error)
                }
            }
            getShoes();
        }, 5000);

        return () => clearInterval(intervalId);
    }, []);




    // async function deleteShoe(id) {
    //     try {
    //         const response = await fetch(`http://localhost:8080/api/shoes/` + id, {
    //             method: "delete",
    //             headers: {
    //                 "Content-Type": "application/json"
    //             }
    //         });
    //         const data = await response.json()
    //         console.log("this is the data -----", data)
    //     } catch (error) {
    //         console.log("Error", error)
    //     }
    // }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
            {shoes.map(shoe => {
                return (
                <tr key={shoe.id}>
                    <td>{shoe.manufacturer}</td>
                    <td>{shoe.name}</td>
                    <td>{shoe.color}</td>
                    <td>{shoe.url}</td>
                    <td>{shoe.bin}</td>
                    {/* <td>
                        <button onClick={() => deleteShoe(shoe.id) }>Delete</button>
                    </td> */}
                </tr>
                );
            })}
            </tbody>
        </table>
        )
    }

export default ListShoe
