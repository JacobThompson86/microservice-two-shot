import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Hats from './Hats/Hats';
import CreateHat from './Hats/CreateHat';
import CreateShoe from './Shoes/CreateShoe';
import ListShoe from './Shoes/ListShoe'


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/">
              < Route path="new" element={<Hats />} />
          </Route>
          <Route path="hats/">
              < Route path="create" element={<CreateHat />} />
          </Route>
          <Route path="shoes/">
            <Route path="new" element={<ListShoe  />} />
          </Route>
          <Route path="shoes/">
            <Route path="create" element={<CreateShoe  />} />
          </Route>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
