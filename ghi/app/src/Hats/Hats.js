import React, { useState, useEffect } from 'react';

const Hats = () => {

    const [hats,setHat] = useState([])

    useEffect(() => {
        const intervalId = setInterval(() => {
            async function getTask() {
                try {
                    const response = await fetch('http://localhost:8090/api/hats/')
                    const data = await response.json();
                    setHat(data);
                } catch (error) {
                    console.log(error)
                }
            }
            getTask();
        }, 5000);

        return () => clearInterval(intervalId);
    }, []);



    async function deleteTask(id) {
        try {
            const response = await fetch(`http://localhost:8090/api/hats/` + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
            const data = await response.json();
            console.log(data)
        } catch (error) {
            console.error('Error:', error);
        }
    }


    return (
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Style name</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Location</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
        {hats.map(hats => {
            return (
            <tr key={hats.id}>
                <td>{ hats.style_name }</td>
                <td>{ hats.fabric }</td>
                <td>{ hats.color }</td>
                <td>
                    <img
                    src={hats.picture_url}
                    width={150}
                    alt='pic img'
                    />
                </td>
                <td>{ hats.location }</td>
                <td>
                    <button onClick={() => deleteTask(hats.id) }>Delete</button>
                </td>
            </tr>
            );
        })}
        </tbody>
    </table>
    )
}
export default Hats
