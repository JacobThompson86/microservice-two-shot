import React, {useState,useEffect} from 'react';


const CreateHat = () => {

    const [style_name,setName] = useState('')
    const [fabric,setFabric] = useState('')
    const [color,setColor] = useState('')
    const [picture,setPicture] = useState('')
    const [locations,setLocations] = useState([])
    const [location,setLocation] = useState('')

    const handleStyleChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handlePictureChange = (event) => {
        const value = event.target.value
        setPicture(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.style_name = style_name
        data.fabric = fabric
        data.color = color
        data.picture = picture
        data.location = location

        const hatUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(hatUrl, fetchConfig)
        if(response.ok) {
            const newHat = await response.json()
            console.log(newHat)

            setName('')
            setFabric('')
            setColor('')
            setPicture('')
            setLocation('')
        }
    }
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/locations/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations)
            }
        }

        useEffect(() => {
            fetchData();
    }, []);
    return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Hat</h1>
                <form onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                    <input onChange={handleStyleChange} placeholder="Name" required type="text" name="name" className="form-control" value={style_name} />
                    <label htmlFor="style_name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFabricChange}  required type="text" className="form-control" value={fabric} />
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} placeholder="Color" required type="text" className="form-control" value={color} />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} placeholder="Picture" required type="link" className="form-control" value={picture} />
                    <label htmlFor="picture">Picture</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} value={location}required className="form-select" >
                    <option>Choose a Location</option>
                    {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.closet_name}
                                        </option>
                                    )
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>

            </div>
            </div>
        </div>
    )
}

export default CreateHat
