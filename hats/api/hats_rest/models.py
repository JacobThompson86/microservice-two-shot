from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Hat(models.Model):
    style_name = models.CharField(max_length=25)
    fabric = models.CharField(max_length=25)
    color = models.CharField(max_length=10)
    picture = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style_name
