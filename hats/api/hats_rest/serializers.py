from rest_framework import serializers
from .models import Hat


class HatSerializers(serializers.ModelSerializer):
    class Meta:
        model = Hat
        fields = ["id","style_name", "fabric", "color","picture","location"]
