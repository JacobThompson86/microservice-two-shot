from django.urls import path
from . import views

urlpatterns = [
    path("hats/", views.hat_list, name="hat_list" ),
    path("hats/<int:id>", views.hat_details, name="hat_details"),
]
