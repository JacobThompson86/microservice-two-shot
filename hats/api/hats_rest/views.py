from .models import Hat
from .serializers import HatSerializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

# GET POST and DELETE

@api_view(['GET','POST'])
def hat_list(request):

    if request.method == 'GET':
        hats = Hat.objects.all()
        serializer = HatSerializers(hats, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = HatSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','DELETE'])
def hat_details(request,id):

    try:
        hats = Hat.objects.get(id=id)
    except Hat.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = HatSerializers(hats)
        return Response(serializer.data)
    elif request.method == "DELETE":
        hats.delete()
        return Response(status=status.HTTP_200_OK)
